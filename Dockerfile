FROM nicosingh/rpi-opencv:latest

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# Download required software
WORKDIR /
RUN apt-get update -y && \
  apt-get install git && \
  rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/biometrics/openbr.git
RUN cd /openbr && \
    git checkout v1.1.0 && \
    git submodule init && \
    git submodule update

# Install OpenBR
RUN mkdir -p /openbr/build && \
    cd /openbr/build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    make -j4 && \
    make install

# Expose volume to share images between our host and container
VOLUME /images
