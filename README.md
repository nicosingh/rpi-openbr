[![pipeline status](https://gitlab.com/nicosingh/rpi-openbr/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-openbr/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-openbr.svg)](https://hub.docker.com/r/nicosingh/rpi-openbr/)

# About

Docker image of Raspbian with OpenBR software already installed. It depends of another image of OpenCV, which is based on raspbian.

# How to use this Docker image?

This is a raspbian-based image, and doesn't depend of any particular service to work. In this case makes sense to open a bash shell, with a command like:

`docker run -it -v /tmp/images/:/images nicosingh/rpi-openbr bash`

Where:

`docker run -it`: means to run a new Docker container with interactive mode

`-v /tmp/images:/images`: means we are going to map the directory /tmp/images from our host computer to /images folder inside our container. You can change /tmp/images directory with whatever you want to process using OpenBR.

`nicosingh/rpi-openbr bash`: means to open a BASH shell in our new container
